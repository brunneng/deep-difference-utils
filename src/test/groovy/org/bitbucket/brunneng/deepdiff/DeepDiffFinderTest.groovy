package org.bitbucket.brunneng.deepdiff

import groovy.transform.EqualsAndHashCode
import org.bibucket.brunneng.deepdiff.Configuration
import org.bibucket.brunneng.deepdiff.DeepDiffFinder
import org.bibucket.brunneng.deepdiff.differences.*
import org.bibucket.brunneng.deepdiff.predicates.GenericPredicate
import org.bibucket.brunneng.deepdiff.predicates.PropertiesComparePredicate
import org.bitbucket.brunneng.introspection.property.PropertyDescription
import spock.lang.Specification

/**
 * @author evvo
 */
class DeepDiffFinderTest extends Specification {

    def 'test find differences - first is null'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        def obj = new Object()
        def differences = deepDiffFinder.findDifferences(null, obj)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == ""
        diff.pathToSecond == ""
        diff.firstValue == null
        diff.secondValue == obj
    }

    def 'test find differences - second is null'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        def obj = new Object()
        def differences = deepDiffFinder.findDifferences(obj, null)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == ""
        diff.pathToSecond == ""
        diff.firstValue == obj
        diff.secondValue == null
    }

    def 'test find differences - only one property exist in first'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        def obj1 = RandomizerUtils.generateObject(Bean1.class)
        def obj2 = new EmptyBean()
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof OnlyOnePathExistsDifference
        OnlyOnePathExistsDifference diff = (OnlyOnePathExistsDifference) differences[0]
        diff.pathToFirst == "value1"
        diff.pathToSecond == null
    }

    def 'test find differences - only one property exist in first - excluded in config'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1.class).excludeProperties("value1")

        def deepDiffFinder = new DeepDiffFinder(c)
        def obj1 = RandomizerUtils.generateObject(Bean1.class)
        def obj2 = new EmptyBean()
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - only one property exist in second'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        def obj1 = new EmptyBean()
        def obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof OnlyOnePathExistsDifference
        OnlyOnePathExistsDifference diff = (OnlyOnePathExistsDifference) differences[0]
        diff.pathToFirst == null
        diff.pathToSecond == "value1"
    }

    def 'test find differences - only one property exist in second - excluded in config'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean1.class).excludeProperties("value1")

        def deepDiffFinder = new DeepDiffFinder(c)
        def obj1 = new EmptyBean()
        def obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean1 obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "value1"
        diff.pathToSecond == "value1"
        diff.firstValue == obj1.value1
        diff.secondValue == obj2.value1
    }

    def 'test find differences - equals difference, excluded'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean1 obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeProperties("value1")
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, excluded first object properties'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean1 obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeFirstObjectProperties("value1")
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, excluded second object properties'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean1 obj2 = RandomizerUtils.generateObject(Bean1.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeSecondObjectProperties("value1")
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in second object'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean8 obj1 = RandomizerUtils.generateObject(Bean8.class)
        Bean16 obj2 = new Bean16()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInSecondObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in first object'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInFirstObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in second object in collection'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean8 b1 = RandomizerUtils.generateObject(Bean8.class)
        Bean16 b2 = new Bean16()
        b2.setValue(b1.getValue())

        Bean17 obj1 = new Bean17()
        obj1.setValues([b1])

        Bean17 obj2 = new Bean17()
        obj2.setValues([b2])

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInSecondObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in first object in collection'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean8 b1 = RandomizerUtils.generateObject(Bean8.class)
        Bean16 b2 = new Bean16()
        b2.setValue(b1.getValue())

        Bean17 obj1 = new Bean17()
        obj1.setValues([b2])

        Bean17 obj2 = new Bean17()
        obj2.setValues([b1])

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInFirstObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in second object in map'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean8 b1 = RandomizerUtils.generateObject(Bean8.class)
        Bean16 b2 = new Bean16()
        b2.setValue(b1.getValue())

        Bean11 obj1 = new Bean11()
        obj1.setMap(["v1" : b1])

        Bean11 obj2 = new Bean11()
        obj2.setMap(["v1" : b2])

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInSecondObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, exclude missing properties in first object in map'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean8 b1 = RandomizerUtils.generateObject(Bean8.class)
        Bean16 b2 = new Bean16()
        b2.setValue(b1.getValue())

        Bean11 obj1 = new Bean11()
        obj1.setMap(["v1" : b2])

        Bean11 obj2 = new Bean11()
        obj2.setMap(["v1" : b1])

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeMissingPropertiesInFirstObject()
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, first excluded in config'() {
        when:
        def c = new Configuration()
        def deepDiffFinder = new DeepDiffFinder(c)
        c.beanOfClass(Bean1.class).excludeProperties("value1")

        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean13 obj2 = RandomizerUtils.generateObject(Bean13.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, second excluded in config'() {
        when:
        def c = new Configuration()
        def deepDiffFinder = new DeepDiffFinder(c)
        c.beanOfClass(Bean13.class).excludeProperties("value1")

        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean13 obj2 = RandomizerUtils.generateObject(Bean13.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals same'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean1 obj1 = RandomizerUtils.generateObject(Bean1.class)
        Bean1 obj2 = RandomizerUtils.generateObject(Bean1.class)
        obj2.value1 = obj1.value1
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - nested objects, equals difference'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean2 obj1 = RandomizerUtils.generateObject(Bean2.class)
        Bean2 obj2 = RandomizerUtils.generateObject(Bean2.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "bean1.value1"
        diff.pathToSecond == "bean1.value1"
        diff.firstValue == obj1.bean1.value1
        diff.secondValue == obj2.bean1.value1
    }

    def 'test find differences - nested objects, request compare by equals on first'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean2.class).applyCompareByEqualsOnProperties("bean1")

        def deepDiffFinder = new DeepDiffFinder(c)
        Bean2 obj1 = RandomizerUtils.generateObject(Bean2.class)
        Bean18 obj2 = RandomizerUtils.generateObject(Bean18.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "bean1"
        diff.pathToSecond == "bean1"
        diff.firstValue == obj1.bean1
        diff.secondValue == obj2.bean1
    }

    def 'test find differences - nested objects, request compare by equals on second'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean18.class).applyCompareByEqualsOnProperties("bean1")

        def deepDiffFinder = new DeepDiffFinder(c)
        Bean2 obj1 = RandomizerUtils.generateObject(Bean2.class)
        Bean18 obj2 = RandomizerUtils.generateObject(Bean18.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "bean1"
        diff.pathToSecond == "bean1"
        diff.firstValue == obj1.bean1
        diff.secondValue == obj2.bean1
    }

    def 'test find differences - collections, request compare by equals on first'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean4.class).applyCompareByEqualsOnProperties("values")

        def deepDiffFinder = new DeepDiffFinder(c)
        Bean4 obj1 = RandomizerUtils.generateObject(Bean4.class)
        Bean6 obj2 = RandomizerUtils.generateObject(Bean6.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "values"
        diff.pathToSecond == "values"
        diff.firstValue == obj1.values
        diff.secondValue == obj2.values
    }

    def 'test find differences - collections, request compare by equals on second'() {
        when:
        def c = new Configuration()
        c.beanOfClass(Bean6.class).applyCompareByEqualsOnProperties("values")

        def deepDiffFinder = new DeepDiffFinder(c)
        Bean4 obj1 = RandomizerUtils.generateObject(Bean4.class)
        Bean6 obj2 = RandomizerUtils.generateObject(Bean6.class)
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof EqualsDifference
        EqualsDifference diff = (EqualsDifference) differences[0]
        diff.pathToFirst == "values"
        diff.pathToSecond == "values"
        diff.firstValue == obj1.values
        diff.secondValue == obj2.values
    }

    def 'test find differences - nested generic objects, only one property exist in first'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean3<Bean1> obj1 = new Bean3<>()
        obj1.setBean(RandomizerUtils.generateObject(Bean1.class))

        Bean3<EmptyBean> obj2 = new Bean3<>()
        obj2.setBean(RandomizerUtils.generateObject(EmptyBean.class))

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences[0] instanceof OnlyOnePathExistsDifference
        OnlyOnePathExistsDifference diff = (OnlyOnePathExistsDifference) differences[0]
        diff.pathToFirst == "bean.value1"
        diff.pathToSecond == null
    }

    def 'test find differences - same ordered collections'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean4 obj1 = new Bean4()
        obj1.values = ["1", "2", "3"]
        Bean4 obj2 = new Bean4()
        obj2.values = ["1", "2", "3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - same ordered collections, one is array'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean4 obj1 = new Bean4()
        obj1.values = ["1", "2", "3"]
        Bean6 obj2 = new Bean6()
        obj2.values = ["1", "2", "3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - first collection value is null'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean4 obj1 = new Bean4()
        obj1.values = ["1"]
        Bean4 obj2 = new Bean4()
        obj2.values = [null]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        collectionDifference.firstValue == obj1.values
        collectionDifference.secondValue == obj2.values
        collectionDifference.differentValues == [new DifferentValuesPair("1", null, [])]
        collectionDifference.valuesOnlyInFirst.isEmpty()
        collectionDifference.valuesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        def equalsDifference = differences.equalsDifferences[0]
        equalsDifference.pathToFirst == "values[0]"
        equalsDifference.pathToSecond == "values[0]"
        equalsDifference.firstValue == "1"
        equalsDifference.secondValue == null
    }

    def 'test find differences - ordered collections only exist in first'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean4 obj1 = new Bean4()
        obj1.values = ["1", "2"]
        Bean4 obj2 = new Bean4()
        obj2.values = ["1"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        collectionDifference.firstValue == obj1.values
        collectionDifference.secondValue == obj2.values
        collectionDifference.differentValues.isEmpty()
        collectionDifference.valuesOnlyInFirst == ["2"]
        collectionDifference.valuesOnlyInSecond.isEmpty()
    }

    def 'test find differences - ordered collections only exist in second'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean4 obj1 = new Bean4()
        obj1.values = ["1"]
        Bean4 obj2 = new Bean4()
        obj2.values = ["1", "2"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        collectionDifference.firstValue == obj1.values
        collectionDifference.secondValue == obj2.values
        collectionDifference.differentValues.isEmpty()
        collectionDifference.valuesOnlyInFirst.isEmpty()
        collectionDifference.valuesOnlyInSecond == ["2"]
    }

    def 'test find differences - different beans in ordered collections'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean5<Bean1> obj1 = new Bean5<>()
        obj1.values = [RandomizerUtils.generateObject(Bean1.class)]
        Bean5<Bean1> obj2 = new Bean5<>()
        obj2.values = [RandomizerUtils.generateObject(Bean1.class)]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        collectionDifference.firstValue == obj1.values
        collectionDifference.secondValue == obj2.values
        collectionDifference.differentValues == [new DifferentValuesPair(obj1.values[0], obj2.values[0], [])]
        collectionDifference.valuesOnlyInFirst.isEmpty()
        collectionDifference.valuesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        def equalsDifference = differences.equalsDifferences[0]
        equalsDifference.pathToFirst == "values[0].value1"
        equalsDifference.pathToSecond == "values[0].value1"
        equalsDifference.firstValue == obj1.values[0].value1
        equalsDifference.secondValue == obj2.values[0].value1
    }

    def 'test find differences - same unordered collections'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean7 obj1 = new Bean7()
        obj1.values = ["1", "2", "3"]
        Bean7 obj2 = new Bean7()
        obj2.values = ["1", "2", "3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - unordered collections has only in first'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean7 obj1 = new Bean7()
        obj1.values = ["1", "2", "3"]
        Bean7 obj2 = new Bean7()
        obj2.values = ["3", "1"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        ((List)collectionDifference.firstValue).toSet() == obj1.values
        ((List)collectionDifference.secondValue).toSet() == obj2.values
        collectionDifference.differentValues.isEmpty()
        collectionDifference.valuesOnlyInFirst == ["2"]
        collectionDifference.valuesOnlyInSecond.isEmpty()
    }

    def 'test find differences - unordered collections has only in second'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean7 obj1 = new Bean7()
        obj1.values = ["3", "2"]
        Bean7 obj2 = new Bean7()
        obj2.values = ["1", "2", "3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.collectionDifferences.size() == 1
        def collectionDifference = differences.collectionDifferences[0]
        collectionDifference.pathToFirst == "values"
        collectionDifference.pathToSecond == "values"
        ((List)collectionDifference.firstValue).toSet() == obj1.values
        ((List)collectionDifference.secondValue).toSet() == obj2.values
        collectionDifference.differentValues.isEmpty()
        collectionDifference.valuesOnlyInFirst.isEmpty()
        collectionDifference.valuesOnlyInSecond == ["1"]
    }

    def 'test find differences - collection with duplication and set'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean7 obj1 = new Bean7()
        obj1.values = ["3", "2"]
        Bean4 obj2 = new Bean4()
        obj2.values = ["2", "3", "3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.getCollectionDifferences().size() == 1
        def cd = differences.getCollectionDifferences()[0]
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"
        cd.valuesOnlyInFirst == []
        cd.valuesOnlyInSecond == ["3"]
    }

    def 'test find differences - exclude collection differences'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean7 obj1 = RandomizerUtils.generateObject(Bean7.class)
        Bean7 obj2 = RandomizerUtils.generateObject(Bean7.class)

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeDifferencesOfType(CollectionDifference.class)
        then:
        differences.isEmpty()
    }

    def 'test find differences - exclude map differences'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean11 obj1 = RandomizerUtils.generateObject(Bean11.class)
        Bean11 obj2 = RandomizerUtils.generateObject(Bean11.class)

        def differences = deepDiffFinder.findDifferences(obj1, obj2).excludeDifferencesOfType(MapDifference.class)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equal, match beans in set by identifier'() {
        when:
        def configuration = new Configuration()
        configuration.beanOfClass(Bean8.class).setIdentifierProperty("id")
        def deepDiffFinder = new DeepDiffFinder(configuration)

        Bean8 b81 = new Bean8()
        b81.setId("1")
        b81.setValue("v1")
        Bean8 b82 = new Bean8()
        b82.setId("2")
        b82.setValue("v2")
        Bean8 b83 = new Bean8()
        b83.setId("1")
        b83.setValue("v1")
        Bean8 b84 = new Bean8()
        b84.setId("2")
        b84.setValue("v2")

        Bean9 obj1 = new Bean9()
        obj1.values = [b81, b82]
        Bean9 obj2 = new Bean9()
        obj2.values = [b84, b83]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - match beans in set by identifier'() {
        when:
        def configuration = new Configuration()
        configuration.beanOfClass(Bean8.class).setIdentifierProperty("id")
        def deepDiffFinder = new DeepDiffFinder(configuration)

        Bean8 b81 = new Bean8()
        b81.setId("id1")
        b81.setValue("v1")
        Bean8 b82 = new Bean8()
        b82.setId("id2")
        b82.setValue("v2")
        Bean8 b83 = new Bean8()
        b83.setId("id1")
        b83.setValue("v3")
        Bean8 b84 = new Bean8()
        b84.setId("id2")
        b84.setValue("v2")

        Bean9 obj1 = new Bean9()
        obj1.values = [b81, b82]
        Bean9 obj2 = new Bean9()
        obj2.values = [b84, b83]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.collectionDifferences.size() == 1
        CollectionDifference cd = differences.collectionDifferences[0]
        cd.differentValues == [new DifferentValuesPair(b81, b83, [])]
        cd.valuesOnlyInFirst.isEmpty()
        cd.valuesOnlyInSecond.isEmpty()
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.firstValue == "v1"
        ed.secondValue == "v3"
        ed.pathToFirst == "values[id1].value"
        ed.pathToSecond == "values[id1].value"
    }

    def 'test find differences - equals, match beans in set by equals/hashcode'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()

        Bean10 b1 = new Bean10()
        b1.setId("id1")
        b1.setValue("v1")
        Bean10 b2 = new Bean10()
        b2.setId("id2")
        b2.setValue("v2")
        Bean10 b3 = new Bean10()
        b3.setId("id1")
        b3.setValue("v1")
        Bean10 b4 = new Bean10()
        b4.setId("id2")
        b4.setValue("v2")

        Bean9 obj1 = new Bean9()
        obj1.values = [b1, b2]
        Bean9 obj2 = new Bean9()
        obj2.values = [b4, b3]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - match beans in set by equals/hashcode'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()

        Bean10 b1 = new Bean10()
        b1.setId("id1")
        b1.setValue("v1")
        Bean10 b2 = new Bean10()
        b2.setId("id2")
        b2.setValue("v2")
        Bean10 b3 = new Bean10()
        b3.setId("id1")
        b3.setValue("v3")
        Bean10 b4 = new Bean10()
        b4.setId("id2")
        b4.setValue("v2")

        Bean9 obj1 = new Bean9()
        obj1.values = [b1, b2]
        Bean9 obj2 = new Bean9()
        obj2.values = [b4, b3]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.collectionDifferences.size() == 1
        CollectionDifference cd = differences.collectionDifferences[0]
        cd.differentValues == [new DifferentValuesPair(b1, b3, [])]
        cd.valuesOnlyInFirst.isEmpty()
        cd.valuesOnlyInSecond.isEmpty()
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.firstValue == "v1"
        ed.secondValue == "v3"
        ed.pathToFirst == "values[Bean10(id=id1;value=v1)].value"
        ed.pathToSecond == "values[Bean10(id=id1;value=v1)].value"
    }


    def 'test find differences - equals, match beans in set by identity'() {
        when:
        def configuration = new Configuration()
        configuration.beanOfClass(Bean8.class).setIdentifierProperty("id")
        def deepDiffFinder = new DeepDiffFinder(configuration)

        Bean1 b1 = new Bean1()
        b1.setValue1("v1")
        Bean1 b2 = new Bean1()
        b2.setValue1("v2")
        Bean1 b3 = new Bean1()
        b3.setValue1("v3")
        Bean1 b4 = new Bean1()
        b4.setValue1("v4")

        Bean9 obj1 = new Bean9()
        obj1.values = [b1, b2, b3, b4]
        Bean9 obj2 = new Bean9()
        obj2.values = [b4, b3, b2, b1]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - match beans in set by identity'() {
        when:
        def configuration = new Configuration()
        configuration.beanOfClass(Bean8.class).setIdentifierProperty("id")
        def deepDiffFinder = new DeepDiffFinder(configuration)

        Bean1 b1 = new Bean1()
        b1.setValue1("v1")
        Bean1 b2 = new Bean1()
        b2.setValue1("v2")
        Bean1 b3 = new Bean1()
        b3.setValue1("v3")
        Bean1 b4 = new Bean1()
        b4.setValue1("v4")

        Bean9 obj1 = new Bean9()
        obj1.values = [b1, b2, b3, b4]
        Bean9 obj2 = new Bean9()
        obj2.values = [b4, b3, b1]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.collectionDifferences.size() == 1
        CollectionDifference cd = differences.collectionDifferences[0]
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"
        ((List)cd.firstValue).toSet() == [b1, b2, b3, b4].toSet()
        ((List)cd.secondValue).toSet() == [b1, b3, b4].toSet()
        cd.differentValues.size() == 0
        cd.valuesOnlyInFirst == [b2]
        cd.valuesOnlyInSecond.isEmpty()
    }

    def 'test find differences - equal, force unordered compare on first'() {
        when:
        Configuration configuration = new Configuration()
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(configuration)
        configuration.beanOfClass(Bean4.class).property("values").applyCompareIgnoringOrder()

        Bean4 obj1 = new Bean4()
        obj1.values = ["v1", "v2", "v3"]
        Bean6 obj2 = new Bean6()
        obj2.values = ["v3", "v1", "v2"].toArray()

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equal, force unordered compare on second'() {
        when:
        Configuration configuration = new Configuration()
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(configuration)
        configuration.beanOfClass(Bean6.class).property("values").applyCompareIgnoringOrder()

        Bean4 obj1 = new Bean4()
        obj1.values = ["v1", "v2", "v3"]
        Bean6 obj2 = new Bean6()
        obj2.values = ["v3", "v1", "v2"].toArray()

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - duplicated values in first, force unordered compare'() {
        when:
        Configuration configuration = new Configuration()
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(configuration)
        configuration.beanOfClass(Bean4.class).property("values").applyCompareIgnoringOrder()

        Bean4 obj1 = new Bean4()
        obj1.values = ["v1", "v1"]
        Bean6 obj2 = new Bean6()
        obj2.values = ["v1"].toArray()

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.getCollectionDifferences().size() == 1
        CollectionDifference cd = differences.getCollectionDifferences()[0]
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"
        cd.valuesOnlyInFirst == ["v1"]
        cd.valuesOnlyInSecond == []
    }

    def 'test find differences - duplicated values in second, force unordered compare'() {
        when:
        Configuration configuration = new Configuration()
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(configuration)
        configuration.beanOfClass(Bean4.class).property("values").applyCompareIgnoringOrder()

        Bean4 obj1 = new Bean4()
        obj1.values = ["v1"]
        Bean6 obj2 = new Bean6()
        obj2.values = ["v1", "v1"].toArray()

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 1
        differences.getCollectionDifferences().size() == 1
        CollectionDifference cd = differences.getCollectionDifferences()[0]
        cd.pathToFirst == "values"
        cd.pathToSecond == "values"
        cd.valuesOnlyInFirst == []
        cd.valuesOnlyInSecond == ["v1"]
    }

    def 'test find differences - equal maps'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()

        Bean11 obj1 = new Bean11();
        obj1.map = ["k1" : "v1", "k2" : "v2"]

        Bean11 obj2 = new Bean11();
        obj2.map = ["k1" : "v1", "k2" : "v2"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - map values are different'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()

        Bean11 obj1 = new Bean11();
        obj1.map = ["k1" : "v1", "k2" : "v2"]

        Bean11 obj2 = new Bean11();
        obj2.map = ["k1" : "v1", "k2" : "v3"]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.mapDifferences.size() == 1
        MapDifference md = differences.mapDifferences[0]
        md.pathToFirst == "map"
        md.pathToSecond == "map"
        md.firstValue == obj1.map
        md.secondValue == obj2.map
        md.differentEntries == [new DifferentValuesPair(new KeyValueEntry("k2", "v2"), new KeyValueEntry("k2", "v3"), [])]
        md.entriesOnlyInFirst.isEmpty()
        md.entriesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.pathToFirst == "map[k2]"
        ed.pathToSecond == "map[k2]"
        ed.firstValue == "v2"
        ed.secondValue == "v3"
    }

    def 'test find differences - map values are different entities'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()

        Bean1 b1 = new Bean1()
        b1.setValue1("v1")

        Bean1 b2 = new Bean1()
        b2.setValue1("v2")

        Bean1 b3 = new Bean1()
        b3.setValue1("v1")

        Bean1 b4 = new Bean1()
        b4.setValue1("v3")

        Bean11 obj1 = new Bean11();
        obj1.map = ["k1" : b1, "k2" : b2]

        Bean11 obj2 = new Bean11();
        obj2.map = ["k1" : b3, "k2" : b4]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.mapDifferences.size() == 1
        MapDifference md = differences.mapDifferences[0]
        md.pathToFirst == "map"
        md.pathToSecond == "map"
        md.firstValue == obj1.map
        md.secondValue == obj2.map
        md.differentEntries == [new DifferentValuesPair(new KeyValueEntry("k2", b2), new KeyValueEntry("k2", b4), [])]
        md.entriesOnlyInFirst.isEmpty()
        md.entriesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.pathToFirst == "map[k2].value1"
        ed.pathToSecond == "map[k2].value1"
        ed.firstValue == "v2"
        ed.secondValue == "v3"
    }

    def 'test find differences - map values are different entities with equals & hashcode'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()

        Bean10 b1 = new Bean10()
        b1.setId("id1")
        b1.setValue("v1")

        Bean10 b2 = new Bean10()
        b2.setId("id2")
        b2.setValue("v1")

        Bean11 obj1 = new Bean11();
        obj1.map = ["k1" : b1]

        Bean11 obj2 = new Bean11();
        obj2.map = ["k1" : b2]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.mapDifferences.size() == 1
        MapDifference md = differences.mapDifferences[0]
        md.pathToFirst == "map"
        md.pathToSecond == "map"
        md.firstValue == obj1.map
        md.secondValue == obj2.map
        md.differentEntries == [new DifferentValuesPair(new KeyValueEntry("k1", b1), new KeyValueEntry("k1", b2), [])]
        md.entriesOnlyInFirst.isEmpty()
        md.entriesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.pathToFirst == "map[k1].id"
        ed.pathToSecond == "map[k1].id"
        ed.firstValue == "id1"
        ed.secondValue == "id2"
    }

    def 'test find differences - map values are different entities with identifier'() {
        when:
        Configuration c = new Configuration()
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        c.beanOfClass(Bean10.class).setIdentifierProperty("id")

        Bean10 b1 = new Bean10()
        b1.setId("id1")
        b1.setValue("v1")

        Bean10 b2 = new Bean10()
        b2.setId("id2")
        b2.setValue("v1")

        Bean11 obj1 = new Bean11();
        obj1.map = ["k1" : b1]

        Bean11 obj2 = new Bean11();
        obj2.map = ["k1" : b2]

        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.size() == 2
        differences.mapDifferences.size() == 1
        MapDifference md = differences.mapDifferences[0]
        md.pathToFirst == "map"
        md.pathToSecond == "map"
        md.firstValue == obj1.map
        md.secondValue == obj2.map
        md.differentEntries == [new DifferentValuesPair(new KeyValueEntry("k1", b1), new KeyValueEntry("k1", b2), [])]
        md.entriesOnlyInFirst.isEmpty()
        md.entriesOnlyInSecond.isEmpty()

        differences.equalsDifferences.size() == 1
        EqualsDifference ed = differences.equalsDifferences[0]
        ed.pathToFirst == "map[k1].id"
        ed.pathToSecond == "map[k1].id"
        ed.firstValue == "id1"
        ed.secondValue == "id2"
    }

    def 'test find differences - beans with recursion'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean12 b1 = new Bean12()
        b1.value = b1

        def differences = deepDiffFinder.findDifferences(b1, b1)
        then:
        differences.isEmpty()
    }

    def 'test find differences - one bean with recursion, other has null'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean12 b1 = new Bean12()
        b1.value = b1

        Bean12 b2 = new Bean12()

        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.size() == 1
        differences.getEqualsDifferences().size() == 1
        EqualsDifference ed = differences.getEqualsDifferences()[0]
        ed.pathToFirst == "value"
        ed.pathToSecond == "value"
        ed.firstValue == b1
        ed.secondValue == null
    }

    def 'test find differences - one bean with recursion'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean12 b1 = new Bean12()
        b1.value = b1
        b1.value2 = "v1"

        Bean12 b2 = new Bean12()
        b2.value2 = "v1"
        Bean12 b3 = new Bean12()
        b3.value2 = "v2"
        b2.value = b3

        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.size() == 2
        differences.getEqualsDifferences().size() == 2
        EqualsDifference ed0 = differences.getEqualsDifferences()[0]
        ed0.pathToFirst == "value.value"
        ed0.pathToSecond == "value.value"
        ed0.firstValue == b1
        ed0.secondValue == null

        EqualsDifference ed1 = differences.getEqualsDifferences()[1]
        ed1.pathToFirst == "value.value2"
        ed1.pathToSecond == "value.value2"
        ed1.firstValue == "v1"
        ed1.secondValue == "v2"
    }

    def 'test find differences - force compare by equals on first'() {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(Bean14.class).applyCompareByEquals()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean14 b1 = new Bean14()
        b1.setValue("v1")
        Bean15 b2 = new Bean15()
        b2.setValue("v2")

        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - force compare by equals on second'() {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(Bean15.class).applyCompareByEquals()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean14 b1 = new Bean14()
        b1.setValue("v1")
        Bean15 b2 = new Bean15()
        b2.setValue("v2")

        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - bean extends from another'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean20 b1 = RandomizerUtils.generateObject(Bean20.class);
        Bean8 b2 = new Bean8();
        b2.setId(b1.getId())
        b2.setValue(b1.getValue())
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - compare properties by custom predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean20 b1 = RandomizerUtils.generateObject(Bean20.class);
        Bean8 b2 = new Bean8();
        b2.setId(b1.getId() + "!")
        b2.setValue(b1.getValue())
        def differences = deepDiffFinder.findDifferences(b1, b2, new PropertiesComparePredicate() {
            @Override
            boolean isShouldCompare(String path1, PropertyDescription property1,
                                    String path2, PropertyDescription property2) {
                if (property1.propertyName == "id") {
                    return false
                }
                return true
            }
        })
        then:
        differences.isEmpty()
    }

    def 'test find differences - compare properties by predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean20 b1 = RandomizerUtils.generateObject(Bean20.class);
        Bean8 b2 = new Bean8();
        b2.setId(b1.getId() + "!")
        b2.setValue(b1.getValue())
        def differences = deepDiffFinder.findDifferences(b1, b2,
                new GenericPredicate().compareOnlyProperties("value"))
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip properties by predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean20 b1 = RandomizerUtils.generateObject(Bean20.class)
        Bean8 b2 = new Bean8()
        b2.setId(b1.getId() + "!")
        b2.setValue(b1.getValue())
        def differences = deepDiffFinder.findDifferences(b1, b2,
                new GenericPredicate().skipProperties("id"))
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip collections by predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean4 b1 = RandomizerUtils.generateObject(Bean4.class)
        Bean4 b2 = RandomizerUtils.generateObject(Bean4.class)
        def differences = deepDiffFinder.findDifferences(b1, b2,
                new GenericPredicate().skipCollections())
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip collections by config'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(new Configuration().skipCollections())
        Bean4 b1 = RandomizerUtils.generateObject(Bean4.class)
        Bean4 b2 = RandomizerUtils.generateObject(Bean4.class)
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip maps by predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean11 b1 = RandomizerUtils.generateObject(Bean11.class)
        Bean11 b2 = RandomizerUtils.generateObject(Bean11.class)
        def differences = deepDiffFinder.findDifferences(b1, b2,
                new GenericPredicate().skipMaps())
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip maps by config'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(new Configuration().skipMaps())
        Bean11 b1 = RandomizerUtils.generateObject(Bean11.class)
        Bean11 b2 = RandomizerUtils.generateObject(Bean11.class)
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip beans by predicate'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder()
        Bean18 b1 = RandomizerUtils.generateObject(Bean18.class)
        Bean18 b2 = RandomizerUtils.generateObject(Bean18.class)
        def differences = deepDiffFinder.findDifferences(b1, b2,
                new GenericPredicate().skipBeans())
        then:
        differences.isEmpty()
    }

    def 'test find differences - skip beans by config'() {
        when:
        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(new Configuration().skipBeans())
        Bean18 b1 = RandomizerUtils.generateObject(Bean18.class)
        Bean18 b2 = RandomizerUtils.generateObject(Bean18.class)
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - consider empty collection in first object and null as equals'() {
        when:
        Configuration c = new Configuration()
        c.considerEmptyCollectionAndNullAsEqual()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean4 b1 = new Bean4();
        b1.values = []
        Bean4 b2 = new Bean4();
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - consider empty collection in second object and null as equals'() {
        when:
        Configuration c = new Configuration()
        c.considerEmptyCollectionAndNullAsEqual()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean4 b1 = new Bean4();
        Bean4 b2 = new Bean4();
        b2.values = []
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - consider empty map in first object and null as equals'() {
        when:
        Configuration c = new Configuration()
        c.considerEmptyMapAndNullAsEqual()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean11 b1 = new Bean11();
        b1.map = new HashMap<>()
        Bean11 b2 = new Bean11();
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - consider empty map in second object and null as equals'() {
        when:
        Configuration c = new Configuration()
        c.considerEmptyMapAndNullAsEqual()

        DeepDiffFinder deepDiffFinder = new DeepDiffFinder(c)
        Bean11 b1 = new Bean11();
        Bean11 b2 = new Bean11();
        b2.map = new HashMap<>()
        def differences = deepDiffFinder.findDifferences(b1, b2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare by properties in first object by predicate'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2,
                new GenericPredicate().compareByPropertiesInFirst())
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare by properties in first object by config'() {
        when:
        def deepDiffFinder = new DeepDiffFinder(new Configuration().compareByPropertiesInFirst())
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare by properties in second object by predicate'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj2, obj1,
                new GenericPredicate().compareByPropertiesInSecond())
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare by properties in second object by config'() {
        when:
        def deepDiffFinder = new DeepDiffFinder(new Configuration().compareByPropertiesInSecond())
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj2, obj1)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare only common properties, missing in first by predicate'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2,
                new GenericPredicate().compareOnlyCommonProperties())
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare only common properties, missing in first by config'() {
        when:
        def deepDiffFinder = new DeepDiffFinder(new Configuration().compareOnlyCommonProperties())
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj1, obj2)
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare only common properties, missing in second by predicate'() {
        when:
        def deepDiffFinder = new DeepDiffFinder()
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj2, obj1,
                new GenericPredicate().compareOnlyCommonProperties())
        then:
        differences.isEmpty()
    }

    def 'test find differences - equals difference, compare only common properties, missing in second by config'() {
        when:
        def deepDiffFinder = new DeepDiffFinder(new Configuration().compareOnlyCommonProperties())
        Bean16 obj1 = RandomizerUtils.generateObject(Bean16.class)
        Bean8 obj2 = new Bean8()
        obj2.setValue(obj1.getValue())
        def differences = deepDiffFinder.findDifferences(obj2, obj1)
        then:
        differences.isEmpty()
    }

    class EmptyBean {
    }

    class Bean1 {
        String value1
    }

    class Bean2 {
        Bean1 bean1
    }

    class Bean3<T> {
        T bean
    }

    class Bean4 {
        List<String> values
    }

    class Bean5<T> {
        List<T> values
    }

    class Bean6 {
        String[] values
    }

    class Bean7 {
        Set<String> values
    }

    class Bean8 {
        String id
        String value
    }

    class Bean9 {
        Set<Object> values;
    }

    @EqualsAndHashCode(includes = "id")
    class Bean10 {
        String id
        String value


        @Override
        public String toString() {
            return "Bean10(id=" + id + ";value=" + value + ")";
        }
    }

    class Bean11 {
        Map<String, Object> map;
    }

    class Bean12 {
        Object value
        Object value2
    }

    class Bean13 {
        String value1
    }

    class Bean14 {
        String value

        boolean equals(Object obj) {
            return true
        }
    }

    class Bean15 {
        String value

        boolean equals(Object obj) {
            return true
        }
    }

    class Bean16 {
        String value
    }

    class Bean17 {
        List<Object> values
    }

    class Bean18 {
        Bean1 bean1
    }

    abstract class Bean19 {
        String id
    }

    class Bean20 extends Bean19 {
        String value
    }

}
