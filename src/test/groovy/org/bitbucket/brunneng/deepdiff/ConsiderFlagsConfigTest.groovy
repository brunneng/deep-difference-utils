package org.bitbucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.Configuration
import spock.lang.Specification

class ConsiderFlagsConfigTest extends Specification {

  def 'test considerEmptyCollectionAndNullAsEqual'() {
    when:
    Configuration c = new Configuration()
    then:
    !c.shouldConsiderEmptyCollectionAndNullAsEqual
    when:
    c.considerEmptyCollectionAndNullAsEqual()
    then:
    c.shouldConsiderEmptyCollectionAndNullAsEqual
  }

  def 'test considerEmptyMapAndNullAsEqual'() {
    when:
    Configuration c = new Configuration()
    then:
    !c.shouldConsiderEmptyMapAndNullAsEqual
    when:
    c.considerEmptyMapAndNullAsEqual()
    then:
    c.shouldConsiderEmptyMapAndNullAsEqual
  }
}
