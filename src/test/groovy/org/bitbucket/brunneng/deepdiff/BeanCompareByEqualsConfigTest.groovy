package org.bitbucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.Configuration
import spock.lang.Specification

class BeanCompareByEqualsConfigTest extends Specification {

  def 'test applyCompareByEquals'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(Bean1.class).applyCompareByEquals()
    then:
    c.beanOfClass(Bean1.class).isRequestedCompareByEquals()
    !c.beanOfClass(AbstractBean.class).isRequestedCompareByEquals()
  }

  def 'test applyCompareByEquals on base class'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(AbstractBean.class).applyCompareByEquals()
    then:
    c.beanOfClass(Bean1.class).isRequestedCompareByEquals()
    c.beanOfClass(AbstractBean.class).isRequestedCompareByEquals()
  }

  static class AbstractBean {
  }

  static class Bean1 extends AbstractBean {

  }
}
