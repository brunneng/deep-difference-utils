package org.bitbucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.Configuration
import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import spock.lang.Specification

/**
 * @author evvo
 */
class ExcludePropertiesConfigTest extends Specification {

    def 'test exclude property'() {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(Bean1.class).property("value2").exclude()
        then:
        c.beanOfClass(Bean1.class).property("value2").isExcluded()
        !c.beanOfClass(Bean1.class).property("value3").isExcluded()
    }

    def 'test exclude parent property'() {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(AbstractBean.class).property("value").exclude()
        then:
        c.beanOfClass(Bean1.class).property("value").isExcluded()
    }

    def 'test exclude property from bean'() {
        when:
        Configuration c = new Configuration()
        def beanConfig = c.beanOfClass(Bean1.class)
        beanConfig.excludeProperties("value", "value3")
        then:
        beanConfig.property("value").isExcluded()
        beanConfig.property("value3").isExcluded()
        !beanConfig.property("value2").isExcluded()
    }

    def 'test exclude property from bean - wrong name'() {
        when:
        Configuration c = new Configuration()
        def beanConfig = c.beanOfClass(Bean1.class)
        beanConfig.excludeProperties("abc")
        then:
        thrown NoSuchPropertyException
    }

    abstract class AbstractBean {
        String value
    }

    class Bean1 extends AbstractBean {
        String value2
        String value3
    }
}
