package org.bitbucket.brunneng.deepdiff;

import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;

/**
 * @author greentea
 */
public class RandomizerUtils {
    private static RandomObjectGenerator randomObjectGenerator;

    static {
        Configuration c = new Configuration();
        c.setSizeOfCollections(2);
        randomObjectGenerator = new RandomObjectGenerator(c);
    }

    public static <T> T generateObject(Class aClass) {
        return randomObjectGenerator.generateRandomObject(aClass);
    }
}
