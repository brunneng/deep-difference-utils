package org.bitbucket.brunneng.deepdiff

import org.unitils.reflectionassert.ReflectionAssert
import spock.lang.Ignore
import spock.lang.Specification

/**
 * @author evvo
 */
class UnitilsTest extends Specification {

    @Ignore
    def 'test reflection equals'() {
        when:
        Bean1 b1 = new Bean1()
        b1.setValue("v1")
        Bean2 b2 = new Bean2()
        b2.setValue("v1")
        then:
        ReflectionAssert.assertReflectionEquals(b1, b2)
        // It failed because objects have different types, which is completely unsuitable behaviour
    }

    class Bean1 {
        String value
    }

    class Bean2 {
        String value
    }
}
