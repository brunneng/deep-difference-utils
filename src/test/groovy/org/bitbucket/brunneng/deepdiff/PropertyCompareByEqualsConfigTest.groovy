package org.bitbucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.Configuration
import org.bitbucket.brunneng.introspection.exceptions.NoSuchPropertyException
import spock.lang.Specification

class PropertyCompareByEqualsConfigTest extends Specification {

  def 'test applyCompareByEquals'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(Bean1.class).property("id").applyCompareByEquals()
    then:
    c.beanOfClass(Bean1.class).property("id").isRequestedCompareByEquals()
    !c.beanOfClass(AbstractBean.class).property("id").isRequestedCompareByEquals()
  }

  def 'test applyCompareByEquals on base class'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(AbstractBean.class).property("id").applyCompareByEquals()
    then:
    c.beanOfClass(Bean1.class).property("id").isRequestedCompareByEquals()
    c.beanOfClass(AbstractBean.class).property("id").isRequestedCompareByEquals()
  }

  def 'test applyCompareByEqualsOnProperties'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(Bean1.class).applyCompareByEqualsOnProperties("id")
    then:
    c.beanOfClass(Bean1.class).property("id").isRequestedCompareByEquals()
    !c.beanOfClass(AbstractBean.class).property("id").isRequestedCompareByEquals()
  }

  def 'test applyCompareByEqualsOnProperties - wrong property name'() {
    when:
    Configuration c = new Configuration()
    c.beanOfClass(Bean1.class).applyCompareByEqualsOnProperties("name")
    then:
    thrown NoSuchPropertyException
  }

  static class AbstractBean {
    String id
  }

  static class Bean1 extends AbstractBean {
  }
}
