package org.bitbucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.Configuration
import org.bibucket.brunneng.deepdiff.exceptions.ConfigurationException
import spock.lang.Specification

/**
 * @author evvo
 */
class CompareIgnoringOrderConfigTest extends Specification {

    def 'test compare ignoring order default value'() {
        when:
        Configuration c = new Configuration()
        then:
        !c.beanOfClass(Bean1.class).property("values").isRequestedCompareIgnoringOrder()
    }

    def 'test apply compare ignoring order default value'() {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(Bean1.class).property("values").applyCompareIgnoringOrder()
        then:
        c.beanOfClass(Bean1.class).property("values").isRequestedCompareIgnoringOrder()
    }

    def 'test request compare ignoring order in wrong property'(String propertyName) {
        when:
        Configuration c = new Configuration()
        c.beanOfClass(Bean1.class).property(propertyName).applyCompareIgnoringOrder()
        then:
        thrown ConfigurationException
        where:
        propertyName | _
        "value"      | _
        "inner"      | _
        "map"        | _
    }

    class Bean1 {
        List<String> values
        String value
        Bean1 inner
        Map<String, String> map
    }
}
