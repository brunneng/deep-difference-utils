package org.bitbucket.brunneng.deepdiff.differences

import org.bibucket.brunneng.deepdiff.differences.EqualsDifference
import spock.lang.Specification

/**
 * @author evvo
 */
class EqualsDifferenceToStringTest extends Specification {

    def 'test EqualsDifference.toString same path'() {
        when:
        def ed = new EqualsDifference("value.name", "John",
                "value.name", "Snow")
        then:
        ed.toString() == "Not equal values in path <\"value.name\">: <John> != <Snow>"
    }

    def 'test EqualsDifference.toString different path'() {
        when:
        def ed = new EqualsDifference("value.name", "John",
                "value.firstName", "Snow")
        then:
        ed.toString() == "The value in path of 1st object: <\"value.name\"> = <John> " +
                "isn't equal to the corresponding value of 2nd object: <\"value.firstName\"> = <Snow>"
    }
}
