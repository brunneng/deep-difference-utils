package org.bitbucket.brunneng.deepdiff.differences


import org.bibucket.brunneng.deepdiff.differences.CollectionDifference
import org.bibucket.brunneng.deepdiff.differences.DifferentValuesPair
import spock.lang.Specification

/**
 * @author evvo
 */
class CollectionDifferenceToStringTest extends Specification {

    def 'test CollectionDifference.toString - same path'() {
        when:
        def diff = new CollectionDifference("bean.values", ["1", "2"], "bean.values", ["1", "3"],
                [new DifferentValuesPair<>("2", "3", [])], ["2"], ["3"])
        then:
        diff.toString() == "Different collections in path <\"bean.values\">: <[1, 2]> != <[1, 3]>" +
                "; different values: <[(2 != 3)]>; values only in 1st: <[2]>; values only in 2nd: <[3]>"
    }

    def 'test CollectionDifference.toString - different path'() {
        when:
        def diff = new CollectionDifference("bean.values", ["1", "2"], "bean2.values", ["1", "3"],
                [new DifferentValuesPair<>("2", "3", [])], ["2"], ["3"])
        then:
        diff.toString() == "Collection in path of 1st object: <\"bean.values\"> = <[1, 2]>" +
                " is different from corresponding collection of 2nd object: <\"bean2.values\"> = <[1, 3]>" +
                "; different values: <[(2 != 3)]>; values only in 1st: <[2]>; values only in 2nd: <[3]>"
    }
}
