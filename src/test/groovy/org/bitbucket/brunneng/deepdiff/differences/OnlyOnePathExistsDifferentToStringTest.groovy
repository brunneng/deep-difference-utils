package org.bitbucket.brunneng.deepdiff.differences

import org.bibucket.brunneng.deepdiff.differences.OnlyOnePathExistsDifference
import spock.lang.Specification

/**
 * @author evvo
 */
class OnlyOnePathExistsDifferentToStringTest extends Specification {

    def 'test OnlyOnePathExistsDifference.toString - only first exists'() {
        when:
        def diff = OnlyOnePathExistsDifference.createPathExistsOnlyInFirst("value.name")
        then:
        diff.toString() == "Path <\"value.name\"> exists only in 1st object"
    }

    def 'test OnlyOnePathExistsDifference.toString - only second exists'() {
        when:
        def diff = OnlyOnePathExistsDifference.createPathExistsOnlyInSecond("value.name")
        then:
        diff.toString() == "Path <\"value.name\"> exists only in 2nd object"
    }
}
