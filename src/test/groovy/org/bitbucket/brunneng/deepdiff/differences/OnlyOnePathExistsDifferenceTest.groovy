package org.bitbucket.brunneng.deepdiff.differences

import org.bibucket.brunneng.deepdiff.differences.OnlyOnePathExistsDifference
import spock.lang.Specification

/**
 * @author evvo
 */
class OnlyOnePathExistsDifferenceTest extends Specification {

    def 'test createPathExistsOnlyInFirst'() {
        when:
        def path = "p1"
        def diff = OnlyOnePathExistsDifference.createPathExistsOnlyInFirst(path)
        then:
        diff.pathToFirst == path
        diff.pathToSecond == null
    }

    def 'test createPathExistsOnlyInSecond'() {
        when:
        def path = "p1"
        def diff = OnlyOnePathExistsDifference.createPathExistsOnlyInSecond(path)
        then:
        diff.pathToFirst == null
        diff.pathToSecond == path
    }
}
