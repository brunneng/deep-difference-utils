package org.bitbucket.brunneng.deepdiff.differences


import org.bibucket.brunneng.deepdiff.differences.DifferentValuesPair
import org.bibucket.brunneng.deepdiff.differences.KeyValueEntry
import org.bibucket.brunneng.deepdiff.differences.MapDifference
import spock.lang.Specification

/**
 * @author evvo
 */
class MapDifferenceToStringTest extends Specification {

    def 'test MapDifference.toString - same path'() {
        when:
        def diff = new MapDifference(
                "bean.map", ["1" : "v1", "2" : "v2"],
                "bean.map", ["1" : "v11", "3" : "v3"],
                [new DifferentValuesPair<>(new KeyValueEntry("1", "v1"), new KeyValueEntry("1", "v11"), [])],
                [new KeyValueEntry("2", "v2")], [new KeyValueEntry("3", "v3")])
        then:
        diff.toString() == "Different maps in path <\"bean.map\">: <{1=v1, 2=v2}> != <{1=v11, 3=v3}>" +
                "; different entries: <[(1=v1 != 1=v11)]>; entries only in 1st: <[2=v2]>; entries only in 2nd: <[3=v3]>"
    }

    def 'test MapDifference.toString - different path'() {
        when:
        def diff = new MapDifference(
                "bean.map", ["1" : "v1", "2" : "v2"],
                "bean1.map", ["1" : "v11", "3" : "v3"],
                [new DifferentValuesPair<>(new KeyValueEntry("1", "v1"), new KeyValueEntry("1", "v11"), [])],
                [new KeyValueEntry("2", "v2")], [new KeyValueEntry("3", "v3")])
        then:
        diff.toString() == "Map in path of 1st object: <\"bean.map\"> = <{1=v1, 2=v2}>" +
                " is different from corresponding map of 2nd object: <\"bean1.map\"> = <{1=v11, 3=v3}>" +
                "; different entries: <[(1=v1 != 1=v11)]>; entries only in 1st: <[2=v2]>; entries only in 2nd: <[3=v3]>"
    }
}
