package org.bibucket.brunneng.deepdiff.exceptions

/**
 * Throw in case of wrong configuration. See message for details.
 */
open class ConfigurationException(message: String): RuntimeException(message) {
}