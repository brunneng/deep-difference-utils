package org.bibucket.brunneng.deepdiff.exceptions

/**
 * Throw in case of exception during comparison. See message for details.
 */
open class ComparisonException(message: String): RuntimeException(message) {
}