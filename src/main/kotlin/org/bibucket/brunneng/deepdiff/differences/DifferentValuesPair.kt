package org.bibucket.brunneng.deepdiff.differences

/**
 * Pair of different values, which is used in collection and map differences.
 *
 * @param firstValue the first compared value
 * @param secondValue the second compared value
 * @param causedDifferences the list of sub-differences which describes why given values are different
 */
class DifferentValuesPair<T>(val firstValue: T, val secondValue: T, val causedDifferences: List<Difference>) {

    internal fun leavingOnlyRemainingDifferences(remainingDifferences: List<Difference>): DifferentValuesPair<T>? {
        val newCausedDifferences = causedDifferences.filter { remainingDifferences.contains(it) }
        return if (newCausedDifferences.isNotEmpty())
            DifferentValuesPair(firstValue, secondValue, newCausedDifferences) else null
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DifferentValuesPair<*>

        if (firstValue != other.firstValue) return false
        if (secondValue != other.secondValue) return false

        return true
    }

    override fun hashCode(): Int {
        var result = firstValue?.hashCode() ?: 0
        result = 31 * result + (secondValue?.hashCode() ?: 0)
        return result
    }

    override fun toString(): String {
        return "($firstValue != $secondValue)"
    }


}