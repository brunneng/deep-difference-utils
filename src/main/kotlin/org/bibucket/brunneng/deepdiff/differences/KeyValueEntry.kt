package org.bibucket.brunneng.deepdiff.differences

/**
 * Auxiliary class which represents the key-value pair.
 *
 * @param key the key
 * @param value the value
 */
data class KeyValueEntry(val key: Any, val value: Any?) {

    override fun toString(): String {
        return "$key=$value"
    }
}