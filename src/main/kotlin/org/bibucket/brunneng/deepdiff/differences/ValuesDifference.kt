package org.bibucket.brunneng.deepdiff.differences

/**
 * Difference between two values.
 *
 * @param pathToFirst the path to the first compared value in the context of the first object
 * @param pathToSecond the path to the second compared value in the context of the second object
 * @param firstValue the first compared value in the first object
 * @param secondValue the second compared value in the second object
 */
abstract class ValuesDifference(pathToFirst: String, val firstValue: Any?,
                                pathToSecond: String, val secondValue: Any?)
    : Difference(pathToFirst, pathToSecond)