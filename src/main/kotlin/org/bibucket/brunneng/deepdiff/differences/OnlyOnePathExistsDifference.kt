package org.bibucket.brunneng.deepdiff.differences

/**
 * Difference which means that only one path to value exists, and therefore, values can't be actually compared.
 * Appears when for example some property exists only in one object, and doesn't exist in another.
 *
 * @param pathToFirst the path to the first compared value in the context of the first object.
 * If it's not 'null' then *pathToSecond* should be 'null'.
 * @param pathToSecond the path to the second compared value in the context of the second object.
 * If it's not 'null' then *pathToFirst* should be 'null'.
 */
class OnlyOnePathExistsDifference private constructor(pathToFirst: String?,
                                                      pathToSecond: String?) : Difference(pathToFirst, pathToSecond) {
    companion object {
        @JvmStatic
        fun createPathExistsOnlyInFirst(path: String): OnlyOnePathExistsDifference {
            return OnlyOnePathExistsDifference(path, null)
        }

        @JvmStatic
        fun createPathExistsOnlyInSecond(path: String): OnlyOnePathExistsDifference {
            return OnlyOnePathExistsDifference(null, path)
        }
    }

    init {
        if (pathToFirst != null && pathToSecond != null) {
            throw IllegalArgumentException("Both pathInFirst=$pathToFirst and pathInSecond=$pathToSecond are not null!")
        }
        else if (pathToFirst == null && pathToSecond == null) {
            throw IllegalArgumentException("Both pathInFirst and pathInSecond are not null")
        }
    }

    override fun toString(): String {
        return if (pathToFirst != null) {
            "Path <\"$pathToFirst\"> exists only in 1st object"
        } else {
            "Path <\"$pathToSecond\"> exists only in 2nd object"
        }
    }
}