package org.bibucket.brunneng.deepdiff.differences

/**
 * List of differences between objects.
 * Allows to exclude some differences, in which you don't interested in, or which are considered by you as 'good'.
 */
class Differences(diffs: Collection<Difference>) : ArrayList<Difference>(diffs) {

    /**
     * @return differences of type [OnlyOnePathExistsDifference]
     */
    fun getOnlyOnePathExistsDifferences(): List<OnlyOnePathExistsDifference> {
        return filterIsInstance<OnlyOnePathExistsDifference>()
    }

    /**
     * @return differences of type [EqualsDifference]
     */
    fun getEqualsDifferences(): List<EqualsDifference> {
        return filterIsInstance<EqualsDifference>()
    }

    /**
     * @return differences of type [CollectionDifference]
     */
    fun getCollectionDifferences(): List<CollectionDifference> {
        return filterIsInstance<CollectionDifference>()
    }

    /**
     * @return differences of type [MapDifference]
     */
    fun getMapDifferences(): List<MapDifference> {
        return filterIsInstance<MapDifference>()
    }

    /**
     * @param properties properites in the first object
     * @return new [Differences] object with excluded all differences in which [Difference.pathToFirst] is equal to
     * any of given properties
     */
    fun excludeFirstObjectProperties(vararg properties: String): Differences {
        return applyFilter { !properties.contains(it.pathToFirst) }
    }

    /**
     * @param properties properties in the second object
     * @return new [Differences] object with excluded all differences in which [Difference.pathToSecond] is equal to
     * any of given properties
     */
    fun excludeSecondObjectProperties(vararg properties: String): Differences {
        return applyFilter { !properties.contains(it.pathToSecond) }
    }

    /**
     * @param properties properties in first or second object
     * @return new [Differences] object with excluded all differences in which [Difference.pathToFirst] or
     * [Difference.pathToSecond] is equal to any of given properties
     */
    fun excludeProperties(vararg properties: String): Differences {
        return applyFilter { !properties.contains(it.pathToFirst) && !properties.contains(it.pathToSecond) }
    }

    /**
     * @return new [Differences] object with excluded all differences related to the fact that some properties from
     * second object are missing in first object
     */
    fun excludeMissingPropertiesInFirstObject(): Differences {
        return applyFilter { if (it is OnlyOnePathExistsDifference) it.pathToFirst != null else true }
    }

    /**
     * @return new [Differences] object with excluded all differences related to the fact that some properties from
     * first object are missing in second object
     */
    fun excludeMissingPropertiesInSecondObject(): Differences {
        return applyFilter { if (it is OnlyOnePathExistsDifference) it.pathToSecond != null else true }
    }

    /**
     * @param differenceType type of difference to exclude
     * @return new [Differences] object with excluded all differences of given type
     */
    fun excludeDifferencesOfType(differenceType: Class<out Difference>): Differences {
        return applyFilter { !differenceType.isAssignableFrom(it::class.java) }
    }

    /**
     * @param predicate predicate to filter differences
     * @return new [Differences] object with excluded all differences for which given predicate is 'false'
     */
    fun applyFilter(predicate: (Difference) -> Boolean): Differences {
        var remainingDifferences = filter { predicate(it) }

        while (true) {
            val currRemainingDifferencesCount = remainingDifferences.size
            remainingDifferences = remainingDifferences.mapNotNull {
                when (it) {
                    is CollectionDifference -> it.leavingOnlyRemainingDifferences(remainingDifferences)
                    is MapDifference -> it.leavingOnlyRemainingDifferences(remainingDifferences)
                    else -> it
                }
            }
            if (remainingDifferences.size == currRemainingDifferencesCount) {
                break
            }
        }

        return Differences(remainingDifferences)
    }
}