package org.bibucket.brunneng.deepdiff.differences

/**
 * Describes the difference between two collections.
 *
 * @param pathToFirst the path to the first compared collection in the context of the first object
 * @param pathToSecond the path to the second compared collection in the context of the first object
 * @param firstCollection the first compared collection in the first object
 * @param secondCollection the second compared collection in the second object
 * @param differentValues the list of pairs of different values
 * @param valuesOnlyInFirst values which exists only in the first collection, but not in the second
 * @param valuesOnlyInSecond values which exists only in the second collection, but not in the first
 */
class CollectionDifference(pathToFirst: String, firstCollection: List<Any?>,
                           pathToSecond: String, secondCollection: List<Any?>,
                           val differentValues: List<DifferentValuesPair<Any?>>,
                           val valuesOnlyInFirst: List<Any?>,
                           val valuesOnlyInSecond: List<Any?>)
    : ValuesDifference(pathToFirst, firstCollection, pathToSecond, secondCollection) {

    internal fun leavingOnlyRemainingDifferences(remainingDifferences: List<Difference>) : CollectionDifference? {
        val newDifferentValues = differentValues.mapNotNull { it.leavingOnlyRemainingDifferences(remainingDifferences) }
        if (newDifferentValues.isEmpty() && valuesOnlyInFirst.isEmpty() && valuesOnlyInSecond.isEmpty()) {
            return null
        }

        return CollectionDifference(
                pathToFirst!!, firstValue as List<Any?>,
                pathToSecond!!, secondValue as List<Any?>,
                newDifferentValues, valuesOnlyInFirst, valuesOnlyInSecond)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        if (pathToFirst == pathToSecond) {
            sb.append("Different collections in path <\"$pathToFirst\">: <$firstValue> != <$secondValue>")
        }
        else {
            sb.append("Collection in path of 1st object: <\"$pathToFirst\"> = <$firstValue> is different from " +
                    "corresponding collection of 2nd object: <\"${pathToSecond}\"> = <$secondValue>")
        }
        if (differentValues.isNotEmpty()) {
            sb.append("; different values: <$differentValues>")
        }
        if (valuesOnlyInFirst.isNotEmpty()) {
            sb.append("; values only in 1st: <$valuesOnlyInFirst>")
        }
        if (valuesOnlyInSecond.isNotEmpty()) {
            sb.append("; values only in 2nd: <$valuesOnlyInSecond>")
        }
        return sb.toString()
    }
}