package org.bibucket.brunneng.deepdiff.differences

/**
 * Describes the difference between two maps.
 *
 * @param pathToFirst the path to the first compared map in the context of the first object
 * @param pathToSecond the path to the second compared map in the context of the first object
 * @param firstMap the first compared map in the first object
 * @param secondMap the second compared map in the second object
 * @param differentEntries the list of pairs of different entries
 * @param entriesOnlyInFirst entries which exists only in the first map, but not in the second
 * @param entriesOnlyInSecond entries which exists only in the second map, but not in the first
 */
class MapDifference(pathToFirst: String, firstMap: Map<Any, Any?>,
                    pathToSecond: String, secondMap: Map<Any, Any?>,
                    val differentEntries: List<DifferentValuesPair<KeyValueEntry>>,
                    val entriesOnlyInFirst: List<KeyValueEntry>,
                    val entriesOnlyInSecond: List<KeyValueEntry>)
    : ValuesDifference(pathToFirst, firstMap, pathToSecond, secondMap) {

    internal fun leavingOnlyRemainingDifferences(remainingDifferences: List<Difference>) : MapDifference? {
        val newDifferentEntries = differentEntries.mapNotNull { it.leavingOnlyRemainingDifferences(remainingDifferences) }
        if (newDifferentEntries.isEmpty() && entriesOnlyInFirst.isEmpty() && entriesOnlyInSecond.isEmpty()) {
            return null
        }

        return MapDifference(
                pathToFirst!!, firstValue as Map<Any, Any?>,
                pathToSecond!!, secondValue as Map<Any, Any?>,
                newDifferentEntries, entriesOnlyInFirst, entriesOnlyInSecond)
    }

    override fun toString(): String {
        val sb = StringBuilder()
        if (pathToFirst == pathToSecond) {
            sb.append("Different maps in path <\"$pathToFirst\">: <$firstValue> != <$secondValue>")
        }
        else {
            sb.append("Map in path of 1st object: <\"$pathToFirst\"> = <$firstValue> is different from " +
                    "corresponding map of 2nd object: <\"${pathToSecond}\"> = <$secondValue>")
        }
        if (differentEntries.isNotEmpty()) {
            sb.append("; different entries: <$differentEntries>")
        }
        if (entriesOnlyInFirst.isNotEmpty()) {
            sb.append("; entries only in 1st: <$entriesOnlyInFirst>")
        }
        if (entriesOnlyInSecond.isNotEmpty()) {
            sb.append("; entries only in 2nd: <$entriesOnlyInSecond>")
        }
        return sb.toString()
    }
}