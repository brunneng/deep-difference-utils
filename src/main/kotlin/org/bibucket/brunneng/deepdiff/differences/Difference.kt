package org.bibucket.brunneng.deepdiff.differences

/**
 * Abstract difference between values.
 *
 * @param pathToFirst the path to the first compared value in the context of the first object
 * @param pathToSecond the path to the second compared value in the context of the second object
 */
abstract class Difference(val pathToFirst: String?,
                          val pathToSecond: String?)