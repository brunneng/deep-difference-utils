package org.bibucket.brunneng.deepdiff.differences

/**
 * Difference between two values which were compared by 'equals' and it returned 'false'.
 *
 * @param pathToFirst the path to the first compared value in the context of the first object
 * @param pathToSecond the path to the second compared value in the context of the second object
 * @param firstValue the first compared value in the first object
 * @param secondValue the second compared value in the second object
 */
class EqualsDifference(pathToFirst: String, firstValue: Any?,
                       pathToSecond: String, secondValue: Any?)
    : ValuesDifference(pathToFirst, firstValue, pathToSecond, secondValue) {

    override fun toString(): String {
        return if (pathToFirst != pathToSecond) {
            "The value in path of 1st object: <\"$pathToFirst\"> = <$firstValue> isn't equal to the corresponding" +
                    " value of 2nd object: <\"${pathToSecond}\"> = <$secondValue>"
        }
        else {
            "Not equal values in path <\"$pathToFirst\">: <$firstValue> != <$secondValue>"
        }
    }
}