package org.bibucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.differences.*
import org.bibucket.brunneng.deepdiff.exceptions.ComparisonException
import org.bibucket.brunneng.deepdiff.predicates.PropertiesComparePredicate
import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.introspection.property.TypeCategory
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
import kotlin.collections.LinkedHashMap
import kotlin.collections.LinkedHashSet

/**
 * Performs deep (recursive) comparison of objects with possible different classes, but similar structure.
 *
 * Designed to be used in unit tests to compare objects from different layers. For example *Person* entity with
 * *PersonDTO*.
 *
 * The typical way of using it:
 * 1) Create DeepDiffFinder with desired configuration, or default one
 * 2) call [findDifferences] with optional [PropertiesComparePredicate] and get [Differences] object
 * 3) (optional) exclude some differences which are considered by you as 'good'. Note, that better not to exclude
 * differences, but rather exclude fields from being compared. See [PropertiesComparePredicate].
 * 4) Do assert expecting that [Differences] object is empty
 */
class DeepDiffFinder(private val configuration: Configuration = Configuration()) {

    companion object {
        private val NULL_IDENTITY = Object()
    }

    /**
     * @return differences between objects
     */
    fun findDifferences(obj1: Any?, obj2: Any?): Differences {
        return findDifferences(obj1, obj2, null)
    }

    /**
     * Finds differences between objects using predicate to determine if properties should be compared or not
     * @param predicate the predicate to decide if properties should be compared or not
     * @return differences between objects
     */
    fun findDifferences(obj1: Any?, obj2: Any?, predicate: PropertiesComparePredicate?): Differences {
        val context = ComparisonContext(predicate)
        compareNewFrame(context, ComparisonFrame("", obj1, "", obj2))

        return context.differences
    }

    private fun compareNewFrame(context: ComparisonContext, frame: ComparisonFrame) {
        context.pushFrame(frame)
        val path1 = frame.path1
        val obj1 = frame.obj1

        val path2 = frame.path2
        val obj2 = frame.obj2

        if (path1 == null || path2 == null) {
            throw IllegalArgumentException("Unexpectedly path1 or path2 are null!")
        }
        if (configuration.isShouldConsiderEmptyCollectionAndNullAsEqual()
                && isOneOfObjectsIsEmptyCollectionAndAnotherIsNull(obj1, obj2)) {
            return
        }
        if (configuration.isShouldConsiderEmptyMapAndNullAsEqual()
                && isOneOfObjectsIsEmptyMapAndAnotherIsNull(obj1, obj2)) {
            return
        }

        if ((obj1 == null && obj2 != null) || (obj1 != null && obj2 == null)) {
            context.addDifference(EqualsDifference(path1, obj1, path2, obj2))
        }
        else if (obj1 != null && obj2 != null) {
            compareExistingPathsAndObjects(path1, obj1, path2, obj2, context)
        }

        context.popFrame()
    }

    private fun isOneOfObjectsIsEmptyCollectionAndAnotherIsNull(obj1: Any?, obj2: Any?): Boolean {
        if (obj1 == null || obj2 == null) {
            val obj1IsEmptyCollection = obj1 != null && configuration.isCollectionType(obj1::class.java) &&
                    extractCollectionValues(obj1).isEmpty()
            val obj2IsEmptyCollection = obj2 != null && configuration.isCollectionType(obj2::class.java) &&
                    extractCollectionValues(obj2).isEmpty()
            if (obj1IsEmptyCollection || obj2IsEmptyCollection) {
                return true
            }
        }
        return false
    }

    private fun isOneOfObjectsIsEmptyMapAndAnotherIsNull(obj1: Any?, obj2: Any?): Boolean {
        if (obj1 == null || obj2 == null) {
            val obj1IsEmptyMap = obj1 is Map<*, *> && obj1.isEmpty()
            val obj2IsEmptyMap = obj2 is Map<*, *> && obj2.isEmpty()
            if (obj1IsEmptyMap || obj2IsEmptyMap) {
                return true
            }
        }
        return false
    }

    private fun compareExistingPathsAndObjects(path1: String, obj1: Any, path2: String, obj2: Any,
                                               context: ComparisonContext) {

        val obj1Visited = context.isVisitedObject1(obj1)
        val obj2Visited = context.isVisitedObject2(obj2)
        if (obj1Visited && obj2Visited) {
            return
        }

        val obj1TypeCategory = configuration.getTypeCategory(obj1::class.java)
        val obj2TypeCategory = configuration.getTypeCategory(obj2::class.java)

        val topFrame = context.getTopFrame()
        val property1 = topFrame.property1
        val property2 = topFrame.property2

        if ((property1 != null && property1.isRequestedCompareByEquals()) ||
                (property2 != null && property2.isRequestedCompareByEquals())) {
            compareByEquals(path1, obj1, path2, obj2, context)
        }
        else if (obj1TypeCategory == TypeCategory.Value && obj2TypeCategory == TypeCategory.Value) {
            compareByEquals(path1, obj1, path2, obj2, context)
        }
        else if (obj1TypeCategory == TypeCategory.Bean && obj2TypeCategory == TypeCategory.Bean) {
            compareBeans(path1, obj1, path2, obj2, context)
        }
        else if (obj1TypeCategory == TypeCategory.Collection && obj2TypeCategory == TypeCategory.Collection) {
            compareCollections(path1, obj1, path2, obj2, context)
        }
        else if (obj1TypeCategory == TypeCategory.Map && obj2TypeCategory == TypeCategory.Map) {
            compareMaps(path1, obj1, path2, obj2, context)
        }
        else {
            compareByEquals(path1, obj1, path2, obj2, context)
        }
    }

    private fun compareBeans(path1: String, beanObj1: Any, path2: String, beanObj2: Any,
                             context: ComparisonContext) {
        val bean1 = configuration.beanOfClass(beanObj1::class.java)
        val bean2 = configuration.beanOfClass(beanObj2::class.java)

        if (bean1.isRequestedCompareByEquals() || bean2.isRequestedCompareByEquals()) {
            compareByEquals(path1, beanObj1, path2, beanObj2, context)
            return
        }

        val bean1PropertyNames = getPropertyNamesWithGetters(bean1)
        val bean2PropertyNames = getPropertyNamesWithGetters(bean2)

        val bean1ExcludedPropertyNames = getExcludedPropertyNames(bean1)
        val bean2ExcludedPropertyNames = getExcludedPropertyNames(bean2)

        processMissingProperties(path1, path2, bean1, bean2, bean1PropertyNames, bean2PropertyNames,
                bean1ExcludedPropertyNames, bean2ExcludedPropertyNames, context)

        for (propertyName in bean1PropertyNames) {
            if (!bean2PropertyNames.contains(propertyName)) {
                continue
            }

            val p1 = bean1.property(propertyName)
            val p2 = bean2.property(propertyName)

            val nextPath1 = appendProperty(path1, propertyName)
            val nextPath2 = appendProperty(path2, propertyName)

            val p1Description = p1.getPropertyDescription()
            val p2Description = p2.getPropertyDescription()
            if (!isShouldCompare(context, nextPath1, p1Description, nextPath2, p2Description)) {
                continue
            }

            val propertyValue1 = p1.getter()!!.get(beanObj1)
            val propertyValue2 = p2.getter()!!.get(beanObj2)
            compareNewFrame(context, ComparisonFrame(
                    p1, nextPath1, propertyValue1,
                    p2, nextPath2, propertyValue2))

        }
    }

    private fun processMissingProperties(path1: String,
                                         path2: String,
                                         bean1: Configuration.Bean,
                                         bean2: Configuration.Bean,
                                         bean1PropertyNames: LinkedHashSet<String>,
                                         bean2PropertyNames: LinkedHashSet<String>,
                                         bean1ExcludedPropertyNames: Set<String>,
                                         bean2ExcludedPropertyNames: Set<String>,
                                         context: ComparisonContext) {
        for (propertyName1 in bean1PropertyNames) {
            if (!bean2PropertyNames.contains(propertyName1) && !bean2ExcludedPropertyNames.contains(propertyName1)) {
                val nextPath = appendProperty(path1, propertyName1)
                if (isShouldCompare(context, nextPath,
                                bean1.property(propertyName1).getPropertyDescription(),
                                null, null)) {
                    context.addDifference(OnlyOnePathExistsDifference.createPathExistsOnlyInFirst(nextPath))
                }
            }
        }
        for (propertyName2 in bean2PropertyNames) {
            if (!bean1PropertyNames.contains(propertyName2) && !bean1ExcludedPropertyNames.contains(propertyName2)) {
                val nextPath = appendProperty(path2, propertyName2)
                if (isShouldCompare(context,null, null,
                                nextPath, bean2.property(propertyName2).getPropertyDescription())) {
                    context.addDifference(OnlyOnePathExistsDifference.createPathExistsOnlyInSecond(nextPath))
                }
            }
        }
    }

    private fun isShouldCompare(context: ComparisonContext,
                                pathToFirst: String?, firstPropertyDescriptor: PropertyDescription?,
                                pathToSecond: String?, secondPropertyDescriptor: PropertyDescription?): Boolean {
        return configuration.getPropertiesComparePredicate().isShouldCompare(
                pathToFirst, firstPropertyDescriptor, pathToSecond, secondPropertyDescriptor) &&
                context.dynamicPredicate?.isShouldCompare(
                        pathToFirst, firstPropertyDescriptor, pathToSecond, secondPropertyDescriptor) ?: true
    }

    private fun getPropertyNamesWithGetters(bean: Configuration.Bean): LinkedHashSet<String> {
        return LinkedHashSet(bean.getPropertiesWithGetter().filter { p -> !p.isExcluded() }
                .map { p -> p.name }.toList())
    }

    private fun getExcludedPropertyNames(bean: Configuration.Bean): Set<String> {
        return HashSet(bean.getProperties().filter { p -> p.isExcluded() }.map { p -> p.name }.toList())
    }

    private fun compareCollections(path1: String, collectionObj1: Any, path2: String, collectionObj2: Any,
                                   context: ComparisonContext) {
        val values1 = extractCollectionValues(collectionObj1)
        val values2 = extractCollectionValues(collectionObj2)

        val differentEntries = ArrayList<DifferentValuesPair<KeyValueEntry>>()
        val entriesOnlyInFirst = ArrayList<KeyValueEntry>()
        val entriesOnlyInSecond = ArrayList<KeyValueEntry>()

        val topFrame = context.getTopFrame()
        val firstRequestedIgnoreOrder = topFrame.property1?.isRequestedCompareIgnoringOrder() == true
        val secondRequestedIgnoreOrder = topFrame.property2?.isRequestedCompareIgnoringOrder() == true

        val orderedCompare = isOrderedCollection(collectionObj1) && isOrderedCollection(collectionObj2) &&
                (!firstRequestedIgnoreOrder && !secondRequestedIgnoreOrder)

        if (orderedCompare) {
            compareCollectionsOrdered(path1, values1, path2, values2, context,
                    differentEntries, entriesOnlyInFirst, entriesOnlyInSecond)
        }
        else {
            compareCollectionsUnordered(path1, values1, path2, values2, context,
                    differentEntries, entriesOnlyInFirst, entriesOnlyInSecond)
        }

        if (differentEntries.isNotEmpty() || entriesOnlyInFirst.isNotEmpty() || entriesOnlyInSecond.isNotEmpty()) {
            context.addDifference(
                    CollectionDifference(path1, values1, path2, values2,
                    differentEntries.map { p ->
                        DifferentValuesPair(p.firstValue.value, p.secondValue.value, p.causedDifferences) },
                    toValuesList(entriesOnlyInFirst),
                    toValuesList(entriesOnlyInSecond)))
        }
    }

    private fun toValuesList(entries: List<KeyValueEntry>): List<Any?> {
        return entries.map{ e -> e.value }
    }

    private fun isOrderedCollection(collectionObj: Any): Boolean {
        return collectionObj !is Set<*>
    }

    private fun compareCollectionsOrdered(path1: String, values1: List<Any?>, path2: String, values2: List<Any?>,
                                          context: ComparisonContext,
                                          differentEntries: ArrayList<DifferentValuesPair<KeyValueEntry>>,
                                          entriesOnlyInFirst: ArrayList<KeyValueEntry>,
                                          entriesOnlyInSecond: ArrayList<KeyValueEntry>) {
        val valuesMultiMap1 = LinkedHashMap<Any, MutableList<Any?>>()
        for ((i, v) in values1.withIndex()) {
            addToMultiMap(valuesMultiMap1, i, v)
        }

        val valuesMultiMap2 = LinkedHashMap<Any, MutableList<Any?>>()
        for ((i, v) in values2.withIndex()) {
            addToMultiMap(valuesMultiMap2, i, v)
        }

        compareMapsInternal(path1, valuesMultiMap1, path2, valuesMultiMap2, context,
                differentEntries, entriesOnlyInFirst, entriesOnlyInSecond)
    }

    private fun compareCollectionsUnordered(path1: String, values1: List<Any?>, path2: String, values2: List<Any?>,
                                            context: ComparisonContext,
                                            differentEntries: ArrayList<DifferentValuesPair<KeyValueEntry>>,
                                            entriesOnlyInFirst: ArrayList<KeyValueEntry>,
                                            entriesOnlyInSecond: ArrayList<KeyValueEntry>) {
        val valuesMultiMap1 = LinkedHashMap<Any, MutableList<Any?>>()
        for (v in values1) {
            addToMultiMap(valuesMultiMap1, getIdentity(v), v)
        }

        val valuesMultiMap2 = LinkedHashMap<Any, MutableList<Any?>>()
        for (v in values2) {
            addToMultiMap(valuesMultiMap2, getIdentity(v), v)
        }

        compareMapsInternal(path1, valuesMultiMap1, path2, valuesMultiMap2, context,
                differentEntries, entriesOnlyInFirst, entriesOnlyInSecond)
    }

    private fun compareMaps(path1: String, mapObj1: Any, path2: String, mapObj2: Any,
                            context: ComparisonContext) {
        if (mapObj1 !is Map<*, *>) {
            throw ComparisonException("Object $mapObj1 expected to be a Map")
        }
        if (mapObj2 !is Map<*, *>) {
            throw ComparisonException("Object $mapObj2 expected to be a Map")
        }

        val differentEntries = ArrayList<DifferentValuesPair<KeyValueEntry>>()
        val entriesOnlyInFirst = ArrayList<KeyValueEntry>()
        val entriesOnlyInSecond = ArrayList<KeyValueEntry>()

        val map1 = mapObj1 as Map<Any, Any?>
        val map2 = mapObj2 as Map<Any, Any?>

        val multiMap1 = LinkedHashMap<Any, MutableList<Any?>>()
        for (e in map1.entries) {
            addToMultiMap(multiMap1, e.key, e.value)
        }

        val multiMap2 = LinkedHashMap<Any, MutableList<Any?>>()
        for (e in map2.entries) {
            addToMultiMap(multiMap2, e.key, e.value)
        }

        compareMapsInternal(path1, multiMap1, path2, multiMap2, context, differentEntries, entriesOnlyInFirst,
                entriesOnlyInSecond)

        if (differentEntries.isNotEmpty() || entriesOnlyInFirst.isNotEmpty() || entriesOnlyInSecond.isNotEmpty()) {
            context.addDifference(MapDifference(path1, map1, path2, map2,
                    differentEntries, entriesOnlyInFirst, entriesOnlyInSecond))
        }
    }

    private fun addToMultiMap(multiMap: LinkedHashMap<Any, MutableList<Any?>>, key: Any, value: Any?) {
        val list = multiMap.getOrPut(key) { ArrayList() }
        list.add(value)
    }

    private fun compareMapsInternal(path1: String, valuesMap1 : Map<Any, List<Any?>>,
                                    path2: String, valuesMap2 : Map<Any, List<Any?>>,
                                    context: ComparisonContext,
                                    differentEntries: ArrayList<DifferentValuesPair<KeyValueEntry>>,
                                    entriesOnlyInFirst: ArrayList<KeyValueEntry>,
                                    entriesOnlyInSecond: ArrayList<KeyValueEntry>) {

        val topFrame = context.getTopFrame()
        for ((identity, values1) in valuesMap1.entries) {
            val nextPath1 = appendKey(path1, identity)
            val v1 = values1.first()

            val values2 = valuesMap2[identity] ?: emptyList()
            if (values2.isNotEmpty()) {
                val v2 = values2.first()

                val differences = context.differences
                val diffsCount = differences.size
                val nextPath2 = appendKey(path2, identity)
                compareNewFrame(context, ComparisonFrame(
                        topFrame.property1, nextPath1, v1, topFrame.property2, nextPath2, v2))
                val different = differences.size > diffsCount
                if (different) {
                    val causedDifferences = differences.subList(diffsCount, differences.size).toList()
                    differentEntries.add(DifferentValuesPair(
                            KeyValueEntry(identity, v1), KeyValueEntry(identity, v2), causedDifferences))
                }
            }

            if (values1.size > values2.size) {
                val duplicatesInFirst = values1.subList(values2.size, values1.size)
                entriesOnlyInFirst.addAll(duplicatesInFirst.map { KeyValueEntry(identity, it) })
            }
            else if (values1.size < values2.size) {
                val duplicatesInSecond = values2.subList(values1.size, values2.size)
                entriesOnlyInSecond.addAll(duplicatesInSecond.map { KeyValueEntry(identity, it) })
            }
        }

        for ((identity, values2) in valuesMap2.entries) {
            if (!valuesMap1.containsKey(identity)) {
                entriesOnlyInSecond.addAll(values2.map { KeyValueEntry(identity, it) })
            }
        }
    }

    private fun getIdentity(value: Any?): Any {
        if (value == null) {
            return NULL_IDENTITY
        }

        if (!configuration.isBeanType(value::class.java)) {
            return value
        }

        val identifierProperty = configuration.beanOfClass(value::class.java).getIdentifierProperty() ?: return value
        return identifierProperty.getter()?.get(value) ?: NULL_IDENTITY
    }

    private fun extractCollectionValues(collectionObj: Any): List<Any?> {
        if (collectionObj is Collection<Any?>) {
            return ArrayList(collectionObj)
        }
        if (collectionObj::class.java.isArray) {
            return (collectionObj as Array<*>).toList()
        }
        else {
            throw ComparisonException("Can't extract values from collection of type ${collectionObj::class.java}")
        }
    }

    private fun appendKey(path: String, key: Any): String {
        return "$path[$key]"
    }

    private fun appendProperty(path: String, property: String): String {
        return if (path.isEmpty()) property else "$path.$property";
    }

    private fun compareByEquals(path1: String, obj1: Any, path2: String, obj2: Any,
                                context: ComparisonContext) {
        if (obj1 != obj2) {
            context.addDifference(EqualsDifference(path1, obj1, path2, obj2))
        }
    }

    private class ComparisonContext(
            val dynamicPredicate: PropertiesComparePredicate? = null,
            val frames: Stack<ComparisonFrame> = Stack(),
            val visitedObjects1: IdentityHashMap<Any, Boolean> = IdentityHashMap(),
            val visitedObjects2: IdentityHashMap<Any, Boolean> = IdentityHashMap(),
            val differences: Differences = Differences(emptyList())) {

        fun addDifference(difference: Difference) {
            differences.add(difference)
        }

        fun isVisitedObject1(obj1: Any): Boolean {
            return isVisitedObjectInternal(visitedObjects1, obj1)
        }
        fun isVisitedObject2(obj2: Any): Boolean {
            return isVisitedObjectInternal(visitedObjects2, obj2)
        }

        fun pushFrame(frame: ComparisonFrame) {
            frames.push(frame)
        }

        fun popFrame() {
            frames.pop()
        }

        fun getTopFrame(): ComparisonFrame {
            return frames.peek()
        }

        private fun isVisitedObjectInternal(visitedObjects: IdentityHashMap<Any, Boolean>, obj: Any): Boolean {
            val objVisited = visitedObjects[obj]
            if (objVisited == null) {
                visitedObjects[obj] = true
            }
            else if (objVisited) {
                return true
            }
            return false
        }
    }

    private class ComparisonFrame(val property1: Configuration.Bean.Property?, val path1: String?, val obj1: Any?,
                                  val property2: Configuration.Bean.Property?, val path2: String?, val obj2: Any?) {
        constructor(path1: String?, obj1: Any?,
                    path2: String?, obj2: Any?): this(null, path1, obj1, null, path2, obj2)
    }

}