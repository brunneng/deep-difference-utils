package org.bibucket.brunneng.deepdiff

import org.bibucket.brunneng.deepdiff.exceptions.ConfigurationException
import org.bibucket.brunneng.deepdiff.predicates.GenericPredicate
import org.bitbucket.brunneng.introspection.AbstractBeanIntrospector
import org.bitbucket.brunneng.introspection.AbstractIntrospector
import org.bitbucket.brunneng.introspection.AbstractProperty
import org.bitbucket.brunneng.introspection.property.Getter
import org.bitbucket.brunneng.introspection.property.Setter
import org.bitbucket.brunneng.introspection.property.TypeCategory
import java.lang.reflect.Type

/**
 * Central class to programmatically configure all comparison process.
 * Prepared configuration is passed to [DeepDiffFinder]. Configuration can be changed after compare but
 * it’s not recommended to do it. If you need to have several different logics of comparison of the same classes then
 * it’s better to create several configuration objects with corresponding [DeepDiffFinder]'s.
 *
 * Through configuration it's possible to access nested *sub-configurations*, such as:
 *
 * [Configuration.Bean],
 *
 * [Configuration.Bean.Property]
 *
 * A  few examples:
 *
 *    configuration.beanOfClass(Order.class)
 *
 * returns [Configuration.Bean] from which you can access all configuration related to Order class.
 *
 *    configuration.beanOfClass(Order.class).property("title")
 *
 * returns [Configuration.Bean.Property] with configurations related to property "title" of Order. This
 * line will throw exception if such property doesn't exist, so you will be able to find possible typos in
 * configuration earlier, before actual comparison.
 *
 * Configuration is **thread safe** and uses fine grained locking to achieve this.
 */
class Configuration : AbstractIntrospector<Configuration.Bean.Property, Configuration.Bean>() {

    private var shouldConsiderEmptyCollectionAndNullAsEqual: Boolean = false
    private var shouldConsiderEmptyMapAndNullAsEqual: Boolean = false
    private val propertiesComparePredicate = GenericPredicate()

    /**
     * Do consider in comparisons that empty collection and null are equal
     */
    fun considerEmptyCollectionAndNullAsEqual(): Configuration {
        shouldConsiderEmptyCollectionAndNullAsEqual = true
        return this
    }

    internal fun isShouldConsiderEmptyCollectionAndNullAsEqual() = shouldConsiderEmptyCollectionAndNullAsEqual

    /**
     * Do consider in comparisons that empty map and null are equal
     */
    fun considerEmptyMapAndNullAsEqual(): Configuration {
        shouldConsiderEmptyMapAndNullAsEqual = true
        return this
    }

    internal fun isShouldConsiderEmptyMapAndNullAsEqual() = shouldConsiderEmptyMapAndNullAsEqual

    /**
     * Skips collections in the first and the second objects.
     */
    fun skipCollections(): Configuration {
        propertiesComparePredicate.skipCollections()
        return this
    }

    /**
     * Skips maps in the first and the second objects.
     */
    fun skipMaps(): Configuration {
        propertiesComparePredicate.skipMaps()
        return this
    }

    /**
     * Skips inner beans in the first and the second objects.
     */
    fun skipBeans(): Configuration {
        propertiesComparePredicate.skipBeans()
        return this
    }

    /**
     * Compare only properties, which are present in the first object,
     * skipping the ones, which exists only in the second
     */
    fun compareByPropertiesInFirst(): Configuration {
        propertiesComparePredicate.compareByPropertiesInFirst()
        return this
    }

    /**
     * Compare only properties, which are present in the second object,
     * skipping the ones, which exists only in the first
     */
    fun compareByPropertiesInSecond(): Configuration {
        propertiesComparePredicate.compareByPropertiesInSecond()
        return this
    }

    /**
     * Compare only properties, which are common for both objects
     */
    fun compareOnlyCommonProperties(): Configuration {
        propertiesComparePredicate.compareOnlyCommonProperties()
        return this
    }

    internal fun getPropertiesComparePredicate() = propertiesComparePredicate

    override fun createBean(beanType: Type): Bean {
        return Bean(beanType)
    }

    /**
     * Configuration for beans of the type [beanType] which applies to all comparisons where this bean participates.
     *
     * Through this configuration it's possible to access nested *sub-configurations* of properties
     * [Configuration.Bean.Property] (see [property]).
     */
    inner class Bean internal constructor(beanType: Type)
        : AbstractBeanIntrospector<Bean.Property, Bean>(beanType, this) {

        private var identifierProperty: Bean.Property? = null
        private var requestedCompareByEquals: Boolean? = null

        /**
         * @return identifier property of current bean, or *null* if no identifier specified
         *
         * @see [Configuration.Bean.Property.markAsIdentifier]
         * @see [Configuration.Bean.setIdentifierProperty]
         */
        fun getIdentifierProperty(): Bean.Property? {
            if (identifierProperty != null) {
                return identifierProperty
            }
            val parent = parentBeanIntrospector
            if (parent != null) {
                val parentIdentifier = parent.getIdentifierProperty()
                if (parentIdentifier != null) {
                    return property(parentIdentifier.name)
                }
            }
            return null
        }

        /**
         * Sets given property as identifier.
         * The identifier will be used to find beans which should be compared in unordered collections.
         *
         * It's common practice to set identifier property on most common superclass of entities, which contains
         * this property. So you do not need to repeat this configuration on every single bean class.
         *
         * For example, you have 3 classes: abstract class Pet, class Dog extends Pet, class Cat extends Pet.
         * Pet has property "id", which should be an identifier. Then use:
         *
         *    configuration.beanOfClass(Pet.class).setIdentifierProperty("id")
         *
         * and it will be automatically applied to Dog and Cat.
         *
         * @param name name of property in bean
         *
         * @throws NoSuchPropertyException if no such property exists in the bean
         *
         * @see BeanFinder
         */
        fun setIdentifierProperty(propertyName: String) {
            property(propertyName).markAsIdentifier()
        }

        /**
         * Excludes all listed properties from being compared
         *
         * @param propertiesToExclude properties to exclude
         *
         * @see [Configuration.Bean.Property.exclude]
         */
        fun excludeProperties(vararg propertiesToExclude: String) {
            for (p in propertiesToExclude) {
                property(p).exclude()
            }
        }

        /**
         * Requests to apply comparison by equals on given properties.
         *
         * @param propertiesToCompareByEquals properties to compare by equals
         *
         * @see [Configuration.Bean.Property.applyCompareByEquals]
         */
        fun applyCompareByEqualsOnProperties(vararg propertiesToCompareByEquals: String) {
            for (p in propertiesToCompareByEquals) {
                property(p).applyCompareByEquals()
            }
        }

        /**
         * Requests for given bean and it's descendants to be always compared by equals,
         * rather then comparing it's properties recursively. It can be requested on the side of first or second
         * object to take effect.
         */
        fun applyCompareByEquals() {
            requestedCompareByEquals = true
        }

        /**
         * @return should this bean or it's descendants be compared only by equals?
         */
        fun isRequestedCompareByEquals(): Boolean {
            if (requestedCompareByEquals == null && parentBeanIntrospector != null) {
                return parentBeanIntrospector.isRequestedCompareByEquals()
            }
            return requestedCompareByEquals ?: false
        }

        override fun createProperty(name: String, getters: List<Getter>, setters: List<Setter>): Property {
            return Property(name, getters, setters)
        }

        /**
         * Configuration for property of bean [beanType] which applies to all comparisons where this bean participates.
         */
        inner class Property(name: String, getters: List<Getter>, setters: List<Setter>)
            : AbstractProperty<Property, Bean>(name, getters, setters, this) {

            private var requestedCompareIgnoringOrder = false
            private var requestedCompareByEquals: Boolean? = null
            private var excluded: Boolean? = null

            /**
             * Marks given property as identifier for instances of current bean.
             * The identifier will be used to find beans which should be compared in unordered collections.
             *
             * It's common practice to set identifier property on most common superclass of entities, which contains
             * this property. So you do not need to repeat this configuration on every single bean class.
             *
             * For example, you have 3 classes: abstract class Pet, class Dog extends Pet, class Cat extends Pet.
             * Pet has property "id", which should be an identifier. Then use:
             *
             *    configuration.beanOfClass(Pet.class).property("id").markAsIdentifier()
             *
             * and it will be automatically applied to Dog and Cat.
             */
            fun markAsIdentifier() {
                identifierProperty = this
            }

            /**
             * Excludes this property from comparison
             */
            fun exclude() {
                excluded = true
            }

            /**
             * @return is this property excluded from comparison?
             */
            fun isExcluded(): Boolean {
                if (excluded == null && parentBeanProperty != null) {
                    return parentBeanProperty.isExcluded()
                }

                return excluded ?: false
            }

            /**
             * Requests for given property to be always compared by equals,
             * rather then comparing it depending on content. It can be requested on the side of first or second
             * object to take effect.
             */
            fun applyCompareByEquals() {
                requestedCompareByEquals = true
            }

            /**
             * @return should this property be compared only by equals?
             */
            fun isRequestedCompareByEquals(): Boolean {
                if (requestedCompareByEquals == null && parentBeanProperty != null) {
                    return parentBeanProperty.isRequestedCompareByEquals()
                }
                return requestedCompareByEquals ?: false
            }

            /**
             * Requests for given collection to be compared ignoring order.
             * Comparison without order uses bean identifier to match beans to be compared.
             * In case if value is not a been or has no identifier then equals/hashcode is used to match values.
             */
            fun applyCompareIgnoringOrder() {
                val typeCategory = getPropertyDescription().propertyAccessorValueType.typeCategory
                if (typeCategory != TypeCategory.Collection) {
                    throw ConfigurationException("Property $name should be collection to apply 'compare " +
                            "ignoring order', but actually it's $typeCategory")
                }
                requestedCompareIgnoringOrder = true
            }

            /**
             * @return is this collection property should be compared ignoring order?
             */
            fun isRequestedCompareIgnoringOrder(): Boolean {
                return requestedCompareIgnoringOrder
            }
        }

    }

}