package org.bibucket.brunneng.deepdiff.predicates

import org.bitbucket.brunneng.introspection.property.PropertyDescription
import org.bitbucket.brunneng.introspection.property.TypeCategory

/**
 * This predicate allows to build criteria by which properties should dynamically be skipped or included during
 * comparison
 */
class GenericPredicate : PropertiesComparePredicate {

    private var shouldSkipCollections: Boolean = false
    private var shouldSkipMaps: Boolean = false
    private var shouldSkipBeans: Boolean = false
    private var shouldCompareByPropertiesInFirst: Boolean = false
    private var shouldCompareByPropertiesInSecond: Boolean = false
    private var shouldCompareOnlyCommonProperties: Boolean = false
    private val skipProperties = HashSet<String>()
    private val compareOnlyProperties = HashSet<String>()

    /**
     * Add properties which should be skipped. Matches properties in the first and the second objects.
     */
    fun skipProperties(vararg propertiesToSkip: String): GenericPredicate {
        skipProperties.addAll(propertiesToSkip)
        return this
    }

    /**
     * Add properties which should be compared, all other will be skipped.
     * Matches properties in the first and the second objects.
     */
    fun compareOnlyProperties(vararg propertiesToCompare: String): GenericPredicate {
        compareOnlyProperties.addAll(propertiesToCompare)
        return this
    }

    /**
     * Skips collections in the first and the second objects.
     */
    fun skipCollections(): GenericPredicate {
        shouldSkipCollections = true
        return this
    }

    /**
     * Skips maps in the first and the second objects.
     */
    fun skipMaps(): GenericPredicate {
        shouldSkipMaps = true
        return this
    }

    /**
     * Skips inner beans in the first and the second objects.
     */
    fun skipBeans(): GenericPredicate {
        shouldSkipBeans = true
        return this
    }

    /**
     * Compare only properties, which are present in the first object,
     * skipping the ones, which exists only in the second
     */
    fun compareByPropertiesInFirst(): GenericPredicate {
        shouldCompareByPropertiesInFirst = true
        return this
    }

    /**
     * Compare only properties, which are present in the second object,
     * skipping the ones, which exists only in the first
     */
    fun compareByPropertiesInSecond(): GenericPredicate {
        shouldCompareByPropertiesInSecond = true
        return this
    }

    /**
     * Compare only properties, which are common for both objects
     */
    fun compareOnlyCommonProperties(): GenericPredicate {
        shouldCompareOnlyCommonProperties = true
        return this
    }

    override fun isShouldCompare(pathToFirst: String?, firstPropertyDescriptor: PropertyDescription?,
                                 pathToSecond: String?, secondPropertyDescriptor: PropertyDescription?): Boolean {
        if (pathToFirst == null || firstPropertyDescriptor == null) {
            val skip = shouldCompareByPropertiesInFirst || shouldCompareOnlyCommonProperties
            return !skip
        }
        if (pathToSecond == null || secondPropertyDescriptor == null) {
            val skip = shouldCompareByPropertiesInSecond || shouldCompareOnlyCommonProperties
            return !skip
        }

        val c1 = firstPropertyDescriptor.propertyAccessorValueType.typeCategory
        val c2 = secondPropertyDescriptor.propertyAccessorValueType.typeCategory
        if (shouldSkipCollections) {
            if (c1 == TypeCategory.Collection || c2 == TypeCategory.Collection) {
                return false
            }
        }
        if (shouldSkipMaps) {
            if (c1 == TypeCategory.Map || c2 == TypeCategory.Map) {
                return false
            }
        }
        if (shouldSkipBeans) {
            if (c1 == TypeCategory.Bean || c2 == TypeCategory.Bean) {
                return false
            }
        }

        if (skipProperties.isNotEmpty()) {
            if (skipProperties.contains(firstPropertyDescriptor.propertyName) ||
                    skipProperties.contains(secondPropertyDescriptor.propertyName)) {
                return false
            }
        }
        if (compareOnlyProperties.isNotEmpty()) {
            return (compareOnlyProperties.contains(firstPropertyDescriptor.propertyName) ||
                    compareOnlyProperties.contains(secondPropertyDescriptor.propertyName))
        }

        return true
    }
}