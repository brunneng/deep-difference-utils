package org.bibucket.brunneng.deepdiff.predicates

import org.bibucket.brunneng.deepdiff.DeepDiffFinder
import org.bitbucket.brunneng.introspection.property.PropertyDescription

/**
 * Called by [DeepDiffFinder] to decide whether properties should be compared or not.
 *
 * You can pass this predicate in method [DeepDiffFinder.findDifferences].
 *
 * The default implementation is [GenericPredicate]. It should be enough for most cases.
 * If not, then you can implement your own predicate with more complex logic.
 */
interface PropertiesComparePredicate {

    /**
     * Makes decision of whether properties should be compared or not.
     * There are 3 possible situations:
     * 1) All 4 arguments are not null
     * 2) [pathToFirst] and [firstPropertyDescriptor] are not null,
     * [pathToSecond] and [secondPropertyDescriptor] are null - in case if the property exists in the first object
     * but not in the second.
     * 3) [pathToFirst] and [firstPropertyDescriptor] are null,
     * [pathToSecond] and [secondPropertyDescriptor] are not null - in case if the property exists in the second object
     * but not in the first.
     *
     * @return Should properties be compared?
     * @param pathToFirst full path to the first compared value in the context of the first object
     * @param firstPropertyDescriptor the property descriptor of the first property
     * @param pathToSecond full path to the second compared value in the context of the second object
     * @param secondPropertyDescriptor the property descriptor of the second property
     */
    fun isShouldCompare(pathToFirst: String?, firstPropertyDescriptor: PropertyDescription?,
                        pathToSecond: String?, secondPropertyDescriptor: PropertyDescription?): Boolean
}